// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3)

import sign.signlink;

import java.math.BigInteger;

public class Buffer extends Class44_Sub3 {

    public static final int[] BITMASK = {
            0, 1, 3, 7, 15, 31, 63, 127, 255, 511,
            1023, 2047, 4095, 8191, 16383, 32767, 65535, 0x1ffff, 0x3ffff, 0x7ffff,
            0xfffff, 0x1fffff, 0x3fffff, 0x7fffff, 0xffffff, 0x1ffffff, 0x3ffffff, 0x7ffffff, 0xfffffff, 0x1fffffff,
            0x3fffffff, 0x7fffffff, -1
    };
    public static final int[] unusedIntArray;
    public static int queueLowCount;
    public static int queueMidCount;
    public static int queueHighCount;
    public static final Class28 queueLow = new Class28(-822);
    public static final Class28 queueMid = new Class28(-822);
    public static final Class28 queueHigh = new Class28(-822);
    public static int unusedInt7;

    static {
        unusedIntArray = new int[256];
        for (int unusedElement = 0; unusedElement < 256; unusedElement++) {
            int unusedValue = unusedElement;
            for (int unusedIndex = 0; unusedIndex < 8; unusedIndex++)
                if ((unusedValue & 1) == 1)
                    unusedValue = unusedValue >>> 1 ^ 0xedb88320;
                else
                    unusedValue >>>= 1;

            unusedIntArray[unusedElement] = unusedValue;
        }

    }

    public boolean unusedBoolean6;
    public int unusedInt4;
    public int unusedInt3;
    public int unusedInt2;
    public boolean unusedBoolean5;
    public int unusedInt;
    public boolean unusedBoolean;
    public boolean unusedBoolean2;
    public boolean unusedBoolean3;
    public boolean unusedBoolean4;
    public int unusedInt5;
    public byte[] data;
    public int position;
    public int bitPosition;
    public Class46 random;
    public char[] unusedCharArray = {
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
            'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
            'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd',
            'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
            'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
            'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7',
            '8', '9', '+', '/'
    };

    public Buffer(byte dummyByte) {
        unusedBoolean6 = true;
        unusedInt4 = 9;
        unusedInt3 = -442;
        unusedInt2 = -41441;
        unusedBoolean5 = true;
        unusedInt = 368;
        unusedBoolean = true;
        unusedBoolean2 = true;
        unusedBoolean3 = false;
        unusedBoolean4 = false;
        unusedInt5 = -186;
        if (dummyByte != 3)
            throw new NullPointerException();
        else {
        }
    }

    public Buffer(byte[] src, byte dummyByte) {
        unusedBoolean6 = true;
        unusedInt4 = 9;
        unusedInt3 = -442;
        unusedInt2 = -41441;
        unusedBoolean5 = true;
        unusedInt = 368;
        unusedBoolean = true;
        unusedBoolean2 = true;
        unusedBoolean3 = false;
        unusedBoolean4 = false;
        unusedInt5 = -186;
        data = src;
        position = 0;
        if (dummyByte != 1) {
            unusedInt5 = 309;
        } else {
        }
    }

    public static Buffer create(int dummyInt, int type) {
        synchronized (queueMid) {
            Buffer buffer = null;
            if (type == 0 && queueLowCount > 0) {
                queueLowCount--;
                buffer = (Buffer) queueLow.method258();
            } else if (type == 1 && queueMidCount > 0) {
                queueMidCount--;
                buffer = (Buffer) queueMid.method258();
            } else if (type == 2 && queueHighCount > 0) {
                queueHighCount--;
                buffer = (Buffer) queueHigh.method258();
            }
            if (buffer != null) {
                buffer.position = 0;
                Buffer tmp = buffer;
                return tmp;
            }
        }
        Buffer buffer = new Buffer((byte) 3);
        if (dummyInt != -7939)
            throw new NullPointerException();
        buffer.position = 0;
        if (type == 0)
            buffer.data = new byte[100];
        else if (type == 1)
            buffer.data = new byte[5000];
        else
            buffer.data = new byte[30000];
        return buffer;
    }

    public void putOpcode(int value) {
        data[position++] = (byte) (value + random.method542());
    }

    public void put1(int value) {
        data[position++] = (byte) value;
    }

    public void put2(int value) {
        data[position++] = (byte) (value >> 8);
        data[position++] = (byte) value;
    }

    public void put2LE(boolean dummyBoolean, int value) {
        data[position++] = (byte) value;
        data[position++] = (byte) (value >> 8);
        if (!dummyBoolean) ;
    }

    public void put3(int value) {
        data[position++] = (byte) (value >> 16);
        data[position++] = (byte) (value >> 8);
        data[position++] = (byte) value;
    }

    public void put4(int value) {
        data[position++] = (byte) (value >> 24);
        data[position++] = (byte) (value >> 16);
        data[position++] = (byte) (value >> 8);
        data[position++] = (byte) value;
    }

    public void put4LE(boolean dummyBoolean, int value) {
        data[position++] = (byte) value;
        data[position++] = (byte) (value >> 8);
        data[position++] = (byte) (value >> 16);
        if (!dummyBoolean) {
        } else {
            data[position++] = (byte) (value >> 24);
        }
    }

    public void put8(long value, int dummyInt) {
        try {
            if (dummyInt != 0)
                unusedInt = 452;
            data[position++] = (byte) (int) (value >> 56);
            data[position++] = (byte) (int) (value >> 48);
            data[position++] = (byte) (int) (value >> 40);
            data[position++] = (byte) (int) (value >> 32);
            data[position++] = (byte) (int) (value >> 24);
            data[position++] = (byte) (int) (value >> 16);
            data[position++] = (byte) (int) (value >> 8);
            data[position++] = (byte) (int) value;
            return;
        } catch (RuntimeException runtimeexception) {
            signlink.reporterror("66663, " + value + ", " + dummyInt + ", " + runtimeexception);
        }
        throw new RuntimeException();
    }

    public void putString(String string) {
        string.getBytes(0, string.length(), data, position);
        position += string.length();
        data[position++] = 10;
    }

    public void putArray(byte[] src, boolean dummyBoolean, int srcOff, int srcLen) {
        if (dummyBoolean)
            unusedBoolean3 = !unusedBoolean3;
        for (int pos = srcOff; pos < srcOff + srcLen; pos++)
            data[position++] = src[pos];

    }

    public void putSize(int size, int dummyInt) {
        if (dummyInt != 0) {
        } else {
            data[position - size - 1] = (byte) size;
        }
    }

    public int get1() {
        return data[position++] & 0xff;
    }

    public byte get1Signed() {
        return data[position++];
    }

    public int get2() {
        position += 2;
        return ((data[position - 2] & 0xff) << 8) + (data[position - 1] & 0xff);
    }

    public int get2Signed() {
        position += 2;
        int value = ((data[position - 2] & 0xff) << 8) + (data[position - 1] & 0xff);
        if (value > 32767)
            value -= 0x10000;
        return value;
    }

    public int get3() {
        position += 3;
        return ((data[position - 3] & 0xff) << 16) + ((data[position - 2] & 0xff) << 8) + (data[position - 1] & 0xff);
    }

    public int get4() {
        position += 4;
        return ((data[position - 4] & 0xff) << 24) + ((data[position - 3] & 0xff) << 16) + ((data[position - 2] & 0xff) << 8) + (data[position - 1] & 0xff);
    }

    public long get8(int dummyInt) {
        long high = (long) get4() & 0xffffffffL;
        long low = (long) get4() & 0xffffffffL;
        if (dummyInt != 0)
            throw new NullPointerException();
        else
            return (high << 32) + low;
    }

    public String getString() {
        int srcPos = position;
        while (data[position++] != 10) ;
        return new String(data, srcPos, position - srcPos - 1);
    }

    public byte[] getStringArray(int dummyInt) {
        int srcPos = position;
        while (data[position++] != 10) ;
        byte[] dst = new byte[position - srcPos - 1];
        if (dummyInt != -32952)
            unusedInt4 = 127;
        if (position - 1 - srcPos >= 0) System.arraycopy(data, srcPos, dst, 0, position - 1 - srcPos);

        return dst;
    }

    public void getArray(int dstLen, byte[] dst, int dstOff, int dummyInt) {
        if (dummyInt != 0) {
            for (int dummyIndex = 1; dummyIndex > 0; dummyIndex++) ;
        }
        for (int pos = dstOff; pos < dstOff + dstLen; pos++)
            dst[pos] = data[position++];

    }

    public void accessBits(byte dummyByte) {
        if (dummyByte != 4) {
        } else {
            bitPosition = position * 8;
        }
    }

    public int getBits(int bits, byte dummyByte) {
        int bytePosition = bitPosition >> 3;
        int msb = 8 - (bitPosition & 7);
        int value = 0;
        bitPosition += bits;
        if (dummyByte != -96)
            unusedInt5 = 0;
        for (; bits > msb; msb = 8) {
            value += (data[bytePosition++] & BITMASK[msb]) << bits - msb;
            bits -= msb;
        }

        if (bits == msb)
            value += data[bytePosition] & BITMASK[msb];
        else
            value += data[bytePosition] >> msb - bits & BITMASK[bits];
        return value;
    }

    public void accessBytes(int dummyInt) {
        position = (bitPosition + 7) / 8;
        dummyInt = 88 / dummyInt;
    }

    public int getSmart1or2Signed() {
        int peek = data[position] & 0xff;
        if (peek < 128)
            return get1() - 64;
        else
            return get2() - 49152;
    }

    public int getSmart1or2() {
        int peek = data[position] & 0xff;
        if (peek < 128)
            return get1();
        else
            return get2() - 32768;
    }

    public void rsaEncrypt(BigInteger modulus, BigInteger exponent, int dummyInt) {
        int start = position;
        position = 0;
        byte[] raw = new byte[start];
        getArray(start, raw, 0, 0);
        BigInteger rawMessage = new BigInteger(raw);
        BigInteger encryptedMessage = rawMessage.modPow(exponent, modulus);
        byte[] encrypted = encryptedMessage.toByteArray();
        position = 0;
        put1(encrypted.length);
        if (dummyInt != 4)
            unusedInt4 = 380;
        putArray(encrypted, false, 0, encrypted.length);
    }
}
