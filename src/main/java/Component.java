// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3)

public class Component {

    public static boolean aBoolean101;
    public static int anInt102;
    public static Component[] instance;
    public static final Class39 modelCache = new Class39(30, 0);
    public static Class39 spriteCache;
    public int[] inventoryObj;
    public int[] inventoryAmount;
    public int sequenceFrame;
    public int sequenceCycle;
    public int id;
    public int layer;
    public int type;
    public int optionType;
    public int clientCode;
    public int width;
    public int height;
    public byte alpha;
    public int x;
    public int y;
    public int[][] script;
    public int[] scriptOperator;
    public int[] scriptOperand;
    public int mouseoverLayer;
    public int scrollSize;
    public int scrollY;
    public boolean hidden;
    public int[] child;
    public int[] childX;
    public int[] childY;
    public int unusedInt;
    public boolean unusedBoolean;
    public boolean inventoryDraggable;
    public boolean inventoryOptions;
    public boolean inventoryUsable;
    public boolean inventoryMoveReplaces;
    public int inventoryMarginX;
    public int inventoryMarginY;
    public Class44_Sub3_Sub1_Sub2[] inventorySprite;
    public int[] inventoryOffsetX;
    public int[] inventoryOffsetY;
    public String[] options;
    public boolean fill;
    public boolean center;
    public boolean shadow;
    public Font font;
    public String message;
    public String activeMessage;
    public int color;
    public int activeColor;
    public int mouseoverColor;
    public int activeMouseoverColor;
    public Class44_Sub3_Sub1_Sub2 sprite;
    public Class44_Sub3_Sub1_Sub2 activeSprite;
    public int modelType;
    public int modelTypeId;
    public int activeModelType;
    public int activeModelTypeId;
    public int sequenceId;
    public int activeSequenceId;
    public int modelZoom;
    public int modelEyePitch;
    public int modelYaw;
    public String optionCircumfix;
    public String optionBase;
    public int optionFlags;
    public String option;
    public Component() {
    }

    public static void unpack(Class47 interfaces, Class47 media, Font[] fonts, int i) {
        spriteCache = new Class39(50000, 0);
        Buffer buffer = new Buffer(interfaces.method546("data", null), (byte) 1);
        int layer;
        for (layer = -1; i >= 0; )
            return;

        int components = buffer.get2();
        instance = new Component[components];
        while (buffer.position < buffer.data.length) {
            int id = buffer.get2();
            if (id == 65535) {
                layer = buffer.get2();
                id = buffer.get2();
            }
            Component component = instance[id] = new Component();
            component.id = id;
            component.layer = layer;
            component.type = buffer.get1();
            component.optionType = buffer.get1();
            component.clientCode = buffer.get2();
            component.width = buffer.get2();
            component.height = buffer.get2();
            component.alpha = (byte) buffer.get1();
            component.mouseoverLayer = buffer.get1();
            if (component.mouseoverLayer != 0)
                component.mouseoverLayer = (component.mouseoverLayer - 1 << 8) + buffer.get1();
            else
                component.mouseoverLayer = -1;
            int operators = buffer.get1();
            if (operators > 0) {
                component.scriptOperator = new int[operators];
                component.scriptOperand = new int[operators];
                for (int operator = 0; operator < operators; operator++) {
                    component.scriptOperator[operator] = buffer.get1();
                    component.scriptOperand[operator] = buffer.get2();
                }

            }
            int scripts = buffer.get1();
            if (scripts > 0) {
                component.script = new int[scripts][];
                for (int script = 0; script < scripts; script++) {
                    int instructions = buffer.get2();
                    component.script[script] = new int[instructions];
                    for (int instruction = 0; instruction < instructions; instruction++)
                        component.script[script][instruction] = buffer.get2();

                }

            }
            if (component.type == 0) {
                component.scrollSize = buffer.get2();
                component.hidden = buffer.get1() == 1;
                int children = buffer.get2();
                component.child = new int[children];
                component.childX = new int[children];
                component.childY = new int[children];
                for (int child = 0; child < children; child++) {
                    component.child[child] = buffer.get2();
                    component.childX[child] = buffer.get2Signed();
                    component.childY[child] = buffer.get2Signed();
                }

            }
            if (component.type == 1) {
                component.unusedInt = buffer.get2();
                component.unusedBoolean = buffer.get1() == 1;
            }
            if (component.type == 2) {
                component.inventoryObj = new int[component.width * component.height];
                component.inventoryAmount = new int[component.width * component.height];
                component.inventoryDraggable = buffer.get1() == 1;
                component.inventoryOptions = buffer.get1() == 1;
                component.inventoryUsable = buffer.get1() == 1;
                component.inventoryMoveReplaces = buffer.get1() == 1;
                component.inventoryMarginX = buffer.get1();
                component.inventoryMarginY = buffer.get1();
                component.inventoryOffsetX = new int[20];
                component.inventoryOffsetY = new int[20];
                component.inventorySprite = new Class44_Sub3_Sub1_Sub2[20];
                for (int slot = 0; slot < 20; slot++) {
                    int placeholderSprite = buffer.get1();
                    if (placeholderSprite == 1) {
                        component.inventoryOffsetX[slot] = buffer.get2Signed();
                        component.inventoryOffsetY[slot] = buffer.get2Signed();
                        String sprite = buffer.getString();
                        if (media != null && sprite.length() > 0) {
                            int idIndex = sprite.lastIndexOf(",");
                            component.inventorySprite[slot] = method186(Integer.parseInt(sprite.substring(idIndex + 1)), true, sprite.substring(0, idIndex), media);
                        }
                    }
                }

                component.options = new String[5];
                for (int option = 0; option < 5; option++) {
                    component.options[option] = buffer.getString();
                    if (component.options[option].length() == 0)
                        component.options[option] = null;
                }

            }
            if (component.type == 3)
                component.fill = buffer.get1() == 1;
            if (component.type == 4 || component.type == 1) {
                component.center = buffer.get1() == 1;
                int fontId = buffer.get1();
                if (fonts != null)
                    component.font = fonts[fontId];
                component.shadow = buffer.get1() == 1;
            }
            if (component.type == 4) {
                component.message = buffer.getString();
                component.activeMessage = buffer.getString();
            }
            if (component.type == 1 || component.type == 3 || component.type == 4)
                component.color = buffer.get4();
            if (component.type == 3 || component.type == 4) {
                component.activeColor = buffer.get4();
                component.mouseoverColor = buffer.get4();
                component.activeMouseoverColor = buffer.get4();
            }
            if (component.type == 5) {
                String sprite = buffer.getString();
                if (media != null && sprite.length() > 0) {
                    int idIndex = sprite.lastIndexOf(",");
                    component.sprite = method186(Integer.parseInt(sprite.substring(idIndex + 1)), true, sprite.substring(0, idIndex), media);
                }
                sprite = buffer.getString();
                if (media != null && sprite.length() > 0) {
                    int idIndex = sprite.lastIndexOf(",");
                    component.activeSprite = method186(Integer.parseInt(sprite.substring(idIndex + 1)), true, sprite.substring(0, idIndex), media);
                }
            }
            if (component.type == 6) {
                int value = buffer.get1();
                if (value != 0) {
                    component.modelType = 1;
                    component.modelTypeId = (value - 1 << 8) + buffer.get1();
                }
                value = buffer.get1();
                if (value != 0) {
                    component.activeModelType = 1;
                    component.activeModelTypeId = (value - 1 << 8) + buffer.get1();
                }
                value = buffer.get1();
                if (value != 0)
                    component.sequenceId = (value - 1 << 8) + buffer.get1();
                else
                    component.sequenceId = -1;
                value = buffer.get1();
                if (value != 0)
                    component.activeSequenceId = (value - 1 << 8) + buffer.get1();
                else
                    component.activeSequenceId = -1;
                component.modelZoom = buffer.get2();
                component.modelEyePitch = buffer.get2();
                component.modelYaw = buffer.get2();
            }
            if (component.type == 7) {
                component.inventoryObj = new int[component.width * component.height];
                component.inventoryAmount = new int[component.width * component.height];
                component.center = buffer.get1() == 1;
                int fontId = buffer.get1();
                if (fonts != null)
                    component.font = fonts[fontId];
                component.shadow = buffer.get1() == 1;
                component.color = buffer.get4();
                component.inventoryMarginX = buffer.get2Signed();
                component.inventoryMarginY = buffer.get2Signed();
                component.inventoryOptions = buffer.get1() == 1;
                component.options = new String[5];
                for (int option = 0; option < 5; option++) {
                    component.options[option] = buffer.getString();
                    if (component.options[option].length() == 0)
                        component.options[option] = null;
                }

            }
            if (component.optionType == 2 || component.type == 2) {
                component.optionCircumfix = buffer.getString();
                component.optionBase = buffer.getString();
                component.optionFlags = buffer.get2();
            }
            if (component.optionType == 1 || component.optionType == 4 || component.optionType == 5 || component.optionType == 6) {
                component.option = buffer.getString();
                if (component.option.length() == 0) {
                    if (component.optionType == 1)
                        component.option = "Ok";
                    if (component.optionType == 4)
                        component.option = "Select";
                    if (component.optionType == 5)
                        component.option = "Select";
                    if (component.optionType == 6)
                        component.option = "Continue";
                }
            }
        }
        spriteCache = null;
    }

    public static void method185(int i, int j, Class44_Sub3_Sub4_Sub4 class44_sub3_sub4_sub4, int k) {
        modelCache.method341();
        if (i != 0)
            anInt102 = 86;
        if (class44_sub3_sub4_sub4 != null && k != 4)
            modelCache.method340(201, (k << 16) + j, class44_sub3_sub4_sub4);
    }

    public static Class44_Sub3_Sub1_Sub2 method186(int i, boolean flag, String s, Class47 class47) {
        long l = (TextUtil.method549(7, s) << 8) + (long) i;
        if (!flag)
            aBoolean101 = !aBoolean101;
        Class44_Sub3_Sub1_Sub2 class44_sub3_sub1_sub2 = (Class44_Sub3_Sub1_Sub2) spriteCache.method339(l);
        if (class44_sub3_sub1_sub2 != null)
            return class44_sub3_sub1_sub2;
        try {
            class44_sub3_sub1_sub2 = new Class44_Sub3_Sub1_Sub2(class47, s, i);
            spriteCache.method340(201, l, class44_sub3_sub1_sub2);
        } catch (Exception _ex) {
            return null;
        }
        return class44_sub3_sub1_sub2;
    }

    public void method182(int i, int j, int k) {
        int l = inventoryObj[i];
        inventoryObj[i] = inventoryObj[j];
        inventoryObj[j] = l;
        l = inventoryAmount[i];
        k = 66 / k;
        inventoryAmount[i] = inventoryAmount[j];
        inventoryAmount[j] = l;
    }

    public Class44_Sub3_Sub4_Sub4 method183(int i, int j, int k, boolean flag) {
        if (k <= 0)
            throw new NullPointerException();
        Class44_Sub3_Sub4_Sub4 class44_sub3_sub4_sub4;
        if (flag)
            class44_sub3_sub4_sub4 = method184(activeModelType, activeModelTypeId);
        else
            class44_sub3_sub4_sub4 = method184(modelType, modelTypeId);
        if (class44_sub3_sub4_sub4 == null)
            return null;
        if (i == -1 && j == -1 && class44_sub3_sub4_sub4.anIntArray1536 == null)
            return class44_sub3_sub4_sub4;
        Class44_Sub3_Sub4_Sub4 class44_sub3_sub4_sub4_1 = new Class44_Sub3_Sub4_Sub4(true, Class11.method211(i, 0) & Class11.method211(j, 0), class44_sub3_sub4_sub4, true, false);
        if (i != -1 || j != -1)
            class44_sub3_sub4_sub4_1.method510(9);
        if (i != -1)
            class44_sub3_sub4_sub4_1.method511(i, -284);
        if (j != -1)
            class44_sub3_sub4_sub4_1.method511(j, -284);
        class44_sub3_sub4_sub4_1.method520(64, 768, -50, -10, -50, true);
        return class44_sub3_sub4_sub4_1;
    }

    public Class44_Sub3_Sub4_Sub4 method184(int i, int j) {
        Class44_Sub3_Sub4_Sub4 class44_sub3_sub4_sub4 = (Class44_Sub3_Sub4_Sub4) modelCache.method339((i << 16) + j);
        if (class44_sub3_sub4_sub4 != null)
            return class44_sub3_sub4_sub4;
        if (i == 1)
            class44_sub3_sub4_sub4 = Class44_Sub3_Sub4_Sub4.method503(j, 6);
        if (i == 2)
            class44_sub3_sub4_sub4 = Class12.method214(j).method217((byte) 0);
        if (i == 3)
            class44_sub3_sub4_sub4 = client.self.method536(true);
        if (i == 4)
            class44_sub3_sub4_sub4 = ObjType.method220(j).method225((byte) 7, 50);
        if (i == 5)
            class44_sub3_sub4_sub4 = null;
        if (class44_sub3_sub4_sub4 != null)
            modelCache.method340(201, (i << 16) + j, class44_sub3_sub4_sub4);
        return class44_sub3_sub4_sub4;
    }

}
