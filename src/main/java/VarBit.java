// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3)

public class VarBit {

    public static final byte aByte577 = 6;
    public static boolean aBoolean578 = true;
    public static int anInt579;
    public static VarBit[] instances;
    public String aString581;
    public int varp;
    public int lsb;
    public int msb;
    public VarBit() {
    }

    public static void method275(boolean flag, Class47 class47) {
        Buffer buffer = new Buffer(class47.method546("varbit.dat", null), (byte) 1);
        anInt579 = buffer.get2();
        if (flag)
            aBoolean578 = !aBoolean578;
        if (instances == null)
            instances = new VarBit[anInt579];
        for (int i = 0; i < anInt579; i++) {
            if (instances[i] == null)
                instances[i] = new VarBit();
            instances[i].method276(buffer, aByte577, i);
        }

        if (buffer.position != buffer.data.length)
            System.out.println("varbit load mismatch");
    }

    public void method276(Buffer buffer, byte byte0, int i) {
        if (byte0 != 6) {
            for (int j = 1; j > 0; j++) ;
        }
        do {
            int k = buffer.get1();
            if (k == 0)
                return;
            if (k == 1) {
                varp = buffer.get2();
                lsb = buffer.get1();
                msb = buffer.get1();
            } else if (k == 10)
                aString581 = buffer.getString();
            else
                System.out.println("Error unrecognised config code: " + k);
        } while (true);
    }

}
