
plugins {
    id("java")
    id("application")
}

group = "com.jagex"
version = "274.0"

repositories {
    mavenCentral()
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(8))
        vendor.set(JvmVendorSpec.AZUL)
    }
}

tasks {
    test {
        useJUnitPlatform()
    }
}
